/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmarie-- <tmarie--@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/06 19:17:30 by tmarie--          #+#    #+#             */
/*   Updated: 2022/05/06 21:36:13 by tmarie--         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*get_next_line(int fd)
{
	char		*buffer;
	char		*cur_line;
	static char	*next = NULL;
	int			read_value;

	if (fd < 0 || BUFFER_SIZE <= 0)
		return (NULL);
	buffer = malloc(sizeof(char) * BUFFER_SIZE + 1);
	if (!buffer)
		return (NULL);
	read_value = 1;
	while ((ft_strchr(next, '\n') == NULL) && read_value != 0)  /// Boucle que pour le depart, ne repasse pas dedans
	{
		read_value = read(fd, buffer, BUFFER_SIZE);
		if (read_value <= 0)
			break ;
		buffer[read_value] = '\0';
		next = ft_strjoin(next, buffer);
	}
	free(buffer);
	cur_line = get_cur_line(next);
	next = get_to_next(next);
	return (cur_line);
}

char	*get_to_next(char	*str)
{
	char	*result;
	int		i;
	int		j;

	if (str == NULL)
		return (NULL);
	i = 0;
	while (str[i] && str[i] != '\n')
		i++;
	if(!str[i] || !str[i + 1])
		return (free(str), NULL);
	if (str[i])
		i++;
	result = malloc(sizeof(char) * ft_strlen(str + i) + 1);
	if (result == NULL)
		return (free(str), NULL);
	j = 0;
	while (str[i])
		result[j++] = str[i++];
	result[j] = '\0';
	free(str);
	return (result);
}

char	*get_cur_line(char	*str)
{
	char	*result;
	int		i;

	if (str == NULL)
		return (NULL);
	result = malloc(sizeof(char) * ft_strlen(str) + 1);
	if (result == NULL)
		return (NULL);
	i = 0;
	while (str[i])
	{
		result[i] = str[i];
		i++;
		if (str[i - 1] == '\n')
			break ;
	}
	result[i] = '\0';
	return (result);
}
